#pattern master

This is a clone of Golan Levin's famous pattern master library which consists of a collection of interpolation curves (or styles) that serve expressive and idiosyncratic purposes for computational design.